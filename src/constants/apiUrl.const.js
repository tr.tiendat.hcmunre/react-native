import {API_VERSIONS} from '../config/system';

const ENDPOINT = {
  LOGIN: 'login',
};

export default {
  LOGIN: `api/${API_VERSIONS}/${ENDPOINT.LOGIN}`,
};
