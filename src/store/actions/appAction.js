import APP_ACTIONS from '../actionTypes/index';

const getAppConfig = () => ({
  type: APP_ACTIONS.GET_CONFIG,
});

export default {
  getAppConfig,
};
