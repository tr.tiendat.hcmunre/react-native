const initState = {
  status: 'success',
  token: null,
  refreshToken: null,
  account: {},
};

const authState = {
  initState,
};

export default authState;
