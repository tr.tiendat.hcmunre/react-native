import Config from 'react-native-config';

export default {
  BASE_URL: 'https://5ee0658d9ed06d001696de92.mockapi.io/',
  API_RETRIES: +Config.API_RETRIES || 1,
  API_TIMEOUT: +Config.API_TIMEOUT || 120000,
  API_VERSIONS: 'v1',
  ENVIRONMENT: Config.ENVIRONMENT,
};
